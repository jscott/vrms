## The Virtual Richard M. Stallman (vrms) package

In the early 1980's <a href="http://www.stallman.org/">Richard M.
Stallman</a> created the <a href="http://www.gnu.org/">GNU project</a>,
whose goal was to provide the world with a <a href=
"http://www.gnu.org/philosophy/free-sw.html">Free Operating System</a>
(with the word "Free" with the same meaning as in "Freedom").

Since then, a lot of Free Software was developed and entire
operating systems based <em>only</em> on Free Software were
created.  Unfortunately, some data is still stored in a format
that is proprietary, secret and non-standard, made by corporations
that want to retain control over the users of the software that
generates such data.

This, of course, is meant to keep the users following the newer
versions of the software that "control" the users' data (so that
the users can still have access to their own creations).

Also, unfortunately, most of the time, the software for
manipulating such data is not Free in the sense desired by Richard
Stallman. Software that does not allow all the freedoms stipulated
by Richard Stallman is called <em>non-free</em> software.

The <tt>vrms</tt> program provides the facility for users
of <a href="http://www.debian.org/">Debian</a>-based Operating
Systems (like, e.g., <a href="http://www.ubuntu.com/">Ubuntu</a>)
to detect if their systems have any non-free software installed,
so that the users can keep their installations only with software
that doesn't pose any legal problems.

The package is now maintained (on
Debian's <a href="https://salsa.debian.org/">salsa</a> system) by
a team of developers including <a href="http://www.gag.com/">Bdale
Garbee</a> (one of the original authors),
<a href="http://www.ime.usp.br/~rbrito/">Rogério Brito</a> and
<a href="http://layer-acht.org/thinking/">Holger Levsen</a>, with
the help of many contributers.

The source code is maintained with a version control system called
<a href="http://git-scm.com/">Git</a>. Anybody can
obtain the source code for the vrms package from <a href=
"https://salsa.debian.org/debian/vrms">salsa.debian.org</a>
and any collaboration is <em>highly</em> appreciated.



This page was made using <a
href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
only.  Free Software is <strong>much more</strong> than zero-cost
software!

Last updated: 2018-10-15
by <a href="http://layer-acht.org/thinking/">Holger Levsen</a>.

